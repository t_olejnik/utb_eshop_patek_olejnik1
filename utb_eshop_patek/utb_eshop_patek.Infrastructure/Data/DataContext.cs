﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using utb_eshop_patek.Domain.Entities.Carts;
using utb_eshop_patek.Domain.Entities.Orders;
using utb_eshop_patek.Domain.Entities.Products;
using utb_eshop_patek.Infrastructure.Extensions;
using utb_eshop_patek.Infrastructure.Identity.Roles;
using utb_eshop_patek.Infrastructure.Identity.Users;

namespace utb_eshop_patek.Infrastructure.Data
{
    public class DataContext : IdentityDbContext<User, Role, int>
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.UseEntityTypeConfiguration();

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.Relational().TableName =
                    entity.Relational().TableName.Replace("AspNet", string.Empty);
            }
        }
    }
}
